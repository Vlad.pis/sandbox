package org.sandbox1

/**
 * Целочисленное сложение
 * @param a1 первый аргумент
 * @param a2 второй аргумент
 * @return сложение
 */

fun add(a1: Int, a2: Int) = a1 + a2
